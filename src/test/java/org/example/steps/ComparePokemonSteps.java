package org.example.steps;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.example.pages.PokemonDBDetailPage;
import org.example.pages.PokemonDBHomePage;
import org.example.pages.SerebilnetDetailPage;
import org.example.pages.SerebilnetHomePage;

import java.util.Map;

import static org.hamcrest.MatcherAssert.*;

public class ComparePokemonSteps {
    PokemonDBHomePage pokemonDBHomePage;
    PokemonDBDetailPage pokemonDBDetailPage;
    SerebilnetHomePage serebilnetPage;
    SerebilnetDetailPage serebilnetDetailPage;

    String pokemonName;


    @Given("^user open the Pokemon Database$")
    public void userOpenThePokemonDatabase() {
        pokemonDBHomePage.open();
    }

    @Then("^user be able to see pokemonNumber on the list$")
    public void userBeAbleToSeePokemonNumberOnTheList(DataTable formDataTable) {
        Map<String, Object> formData = formDataTable.asMap(String.class, Object.class);
        pokemonName = formData.get("pokemonNumber").toString();
        assertThat("pokemon number is not found", pokemonDBHomePage.isPokemonNumberAvailableinPokemonList(pokemonName));
    }

    @When("^user click the pokemon based on pokemon number$")
    public void userClickThePokemonBasedOnPokemonNumber() {
        pokemonDBHomePage.clickThePokemonSelected();
    }

    @Then("^user be able to see <pokemonName> on Pokemon Detail Page$")
    public void userBeAbleToSeePokemonNameOnPokemonDetailPage() {
        assertThat("pokemon name is not found in detail page",pokemonDBDetailPage.getPokemonName().equals(pokemonName));
    }

    @And("^user save the pokemon data from PD$")
    public void userSaveThePokemonDataFromPD() {
        pokemonDBDetailPage.savePokemonToObject();
    }

    @Given("^user open the Serebilnet$")
    public void userOpenTheSerebilnet() {
        serebilnetPage.open();
    }

    @Then("^user be able to see pokemonNumber on the list Serebil$")
    public void userBeAbleToSeePokemonNumberOnTheListSerebil(DataTable formDataTable) {
        Map<String, Object> formData = formDataTable.asMap(String.class, Object.class);
        pokemonName = formData.get("pokemonNumber").toString();
        assertThat("pokemon number is not found", serebilnetPage.isPokemonNumberAvailableinPokemonList(pokemonName));
    }

    @When("^user click the pokemon based on pokemon number Serebil$")
    public void userClickThePokemonBasedOnPokemonNumberSerebil() {
        serebilnetPage.clickThePokemonSelected();
    }

    @Then("^user be able to see <pokemonName> on Pokemon Detail Page Serebil$")
    public void userBeAbleToSeePokemonNameOnPokemonDetailPageSerebil() {
        assertThat("name not found", serebilnetDetailPage.getPokemonName().equals(pokemonName));
    }


    @And("^user compare the name$")
    public void userCompareTheName() {
        assertThat("nama tidak sama", pokemonDBDetailPage.getPokemonStates().get("name").equals(serebilnetDetailPage.getPokemonName()));
    }

    @And("^user compare the number$")
    public void userCompareTheNumber() {
        assertThat("number tidak sama", pokemonDBDetailPage.getPokemonStates().get("number").equals(serebilnetDetailPage.getPokemonNumber()));
    }

    @And("^user compare the type$")
    public void userCompareTheType() {
        assertThat("type tidak sama", serebilnetDetailPage.getPokemonType().toLowerCase().contains(pokemonDBDetailPage.getPokemonStates().get("type").toLowerCase()));
    }

    @And("^user compare the spesies$")
    public void userCompareTheSpesies() {
        assertThat("spesies tidak sama", pokemonDBDetailPage.getPokemonStates().get("spesies").equals(serebilnetDetailPage.getPokemonSpecies()));
    }

    @And("^user compare the weight$")
    public void userCompareTheWeight() {
        assertThat("weight tidak sama", pokemonDBDetailPage.getPokemonStates().get("weight").equals(serebilnetDetailPage.getPokemonWeight()));
    }

    @And("^user compare the height$")
    public void userCompareTheHeight() {
        assertThat("height tidak sama", pokemonDBDetailPage.getPokemonStates().get("height").equals(serebilnetDetailPage.getPokemonHeight()));
    }
}
