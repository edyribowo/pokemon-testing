package org.example.pages;

import cucumber.api.java.en.Given;import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;

import java.util.List;

@DefaultUrl("https://pokemondb.net/pokedex/game/red-blue-yellow")
public class PokemonDBHomePage extends PageObject {

    @FindBy(xpath = "//div[@class='infocard game-red-blue']")
    private WebElementFacade pokemonList;

    @FindBy(xpath = "//div[@class='infocard-list infocard-list-pkmn-lg']")
    private WebElementFacade pokemonSelected;

    String pokemonNumber;

    public boolean isPokemonNumberAvailableinPokemonList(String pokemonNumber) {
        String xpathPokemon = "//a[contains(text(),'" + pokemonNumber + "')]";
        WebElement pokemonSelected = getDriver().findElement(By.xpath(xpathPokemon));
        this.pokemonNumber = pokemonNumber;
        return pokemonSelected.isDisplayed();
    }

    public void clickThePokemonSelected() {
        List<WebElementFacade> listData = findAll(By.xpath("//a[@class='ent-name']"));
        for (WebElementFacade data : listData) {
            if (data.getText().contains(pokemonNumber)) {
                System.out.println(data.getText());
                data.click();
                break;
            }
        }
    }
}