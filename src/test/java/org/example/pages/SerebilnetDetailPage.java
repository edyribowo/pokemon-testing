package org.example.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.yecht.Data;

public class SerebilnetDetailPage extends PageObject {
    @FindBy(xpath = "(//td[@class='tooltabcon'])[1]")
    private WebElementFacade pokemonName;

    @FindBy(xpath = "//body//table[@class='dextab'][1]//following-sibling::td//child::td[2]")
    private WebElementFacade pokemonNumber;

    @FindBy(xpath = "(//td[@class='tooltabcon'])[5]")
    private WebElementFacade pokemonSpecies;

    @FindBy(xpath = "(//td[@class='tooltabcon'])[4]/a")
    private WebElementFacade pokemonType;

    @FindBy(xpath = "(//td[@class='tooltabcon'])[6]")
    private WebElementFacade pokemonHeight;

    @FindBy(xpath = "(//td[@class='tooltabcon'])[7]")
    private WebElementFacade pokemonWeight;

    public String getPokemonName() {
        return pokemonName.getText();
    }

    public String getPokemonNumber() {
        return pokemonNumber.getText().replaceAll("\\D+","");
    }

    public String getPokemonSpecies() {
        return pokemonSpecies.getText();
    }

    public String getPokemonType() {
        System.out.println(pokemonType.getAttribute("href"));
        return pokemonType.getAttribute("href");
    }

    public String getPokemonHeight() {
        return pokemonHeight.getText();
    }

    public String getPokemonWeight() {
        return pokemonWeight.getText();
    }
}
