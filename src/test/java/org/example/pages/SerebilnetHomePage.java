package org.example.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;

import java.util.List;

@DefaultUrl("https://www.serebii.net/fireredleafgreen/kantopokedex.shtml")
public class SerebilnetHomePage extends PageObject {
    String pokemonNumber;
    WebElement pokemonSelected;

    public boolean isPokemonNumberAvailableinPokemonList(String pokemonNumber) {
        String xpathPokemon = "//a[contains(text(),'" + pokemonNumber + "')]";
        pokemonSelected = getDriver().findElement(By.xpath(xpathPokemon));
        this.pokemonNumber = pokemonNumber;
        return pokemonSelected.isDisplayed();
    }

    public void clickThePokemonSelected() {
        pokemonSelected.click();
    }
}
