package org.example.pages;

import cucumber.api.java.en.Then;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PokemonDBDetailPage extends PageObject {

    Map<String, String> pokemonStates = new HashMap<String, String>();

    @FindBy(xpath = "/html[1]/body[1]/main[1]/h1[1]")
    private WebElementFacade pokemonName;

    @FindBy(xpath = "//th[contains(text(),'National')]/following-sibling::td")
    private WebElementFacade pokemonNumber;

    @FindBy(xpath = "//th[contains(text(),'Species')]/following-sibling::td")
    private WebElementFacade pokemonSpecies;

    @FindBy(xpath = "//th[contains(text(),'Type')]/following-sibling::td")
    private WebElementFacade pokemonType;

    @FindBy(xpath = "//th[contains(text(),'Height')]/following-sibling::td")
    private WebElementFacade pokemonHeight;

    @FindBy(xpath = "//th[contains(text(),'Weight')]/following-sibling::td")
    private WebElementFacade pokemonWeight;

    public void savePokemonToObject() {
        pokemonStates.put("name", pokemonName.getText());
        pokemonStates.put("number", pokemonNumber.getText());
        pokemonStates.put("spesies", pokemonSpecies.getText());
        pokemonStates.put("type", pokemonType.getText());
        pokemonStates.put("height", pokemonHeight.getText());
        pokemonStates.put("weight", pokemonWeight.getText());

        System.out.println(pokemonStates.get("weight"));

    }

    public String getPokemonName() {
        return pokemonName.getText();
    }

    public Map<String, String> getPokemonStates() {
        return pokemonStates;
    }
}
