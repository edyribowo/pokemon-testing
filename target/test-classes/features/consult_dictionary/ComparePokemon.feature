Feature: Compare the pokemon detail

  Scenario Outline: Compare the <pokemonNumber>
    Given user open the Pokemon Database
    When user open the website
    Then user be able to see pokemonNumber on the list
      | pokemonNumber | <pokemonNumber> |
    When user click the pokemon based on pokemon number
    Then user be able to see <pokemonName> on Pokemon Detail Page
    And user save the pokemon data from PD
    Given user open the Serebilnet
    When user open the website
    Then user be able to see pokemonNumber on the list Serebil
      | pokemonNumber | <pokemonNumber> |
    When user click the pokemon based on pokemon number Serebil
    Then user be able to see <pokemonName> on Pokemon Detail Page Serebil
    And user compare the name
    And user compare the number
    And user compare the spesies
    And user compare the type
#    And user compare the weight
#    And user compare the height

    Examples:
      | pokemonNumber |
      | Charmander    |
      | Charmeleon    |
      | Wartortle     |
      | Caterpie      |
      | Nidorina      |




